#!/bin/bash

set -e

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
# shellcheck disable=SC1091

wget https://gitlab.com/Linaro/lkft/pipelines/common/-/raw/master/lib.sh
mv lib.sh "${ROOT_DIR}/"
source "${ROOT_DIR}/lib.sh"

git config --global user.name "LKFT Bot"
git config --global user.email lkft-bot@linaro.org

clone_or_update "https://${GIT_USER}:${GIT_PASS}@gitlab.com/mrchapp/mergetestproject.git" origin common

branch="autobranch-$(date -u +"%Y%m%d-%H%M%S")"
merge_date="$(date -u +"%Y-%m-%d")"

echo "${RANDOM}" >> common/README.txt

echo "Committing changes in common..."
git -C common status
git -C common checkout -B "${branch}" origin/master
git -C common add -u .
git -C common commit -s -m "common: Update Tuxplan ID (${merge_date})"
git -C common show

echo "Creating merge-request for common..."
git -C common push -f origin "${branch}"
git -C common push \
  -o merge_request.create \
  -o merge_request.title="[auto] Tuxplan ID: Automerge ${merge_date}" \
  -o merge_request.target=master \
  origin "${branch}"
